#pragma once

#include <lib/ExportMacro.h>
#include <vector>
#include <time.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <algorithm>

#define _USE_MATH_DEFINES
#include <math.h>

class Algorithm
{
protected:
	/** ���������� ��� � ������� �������.*/
	int _N1;
	/** ���������� ��� � ����������� �������.*/
	int _N2;
	/** ��������� �������� �������� �������.*/
	int _time_delay;
	/** ������� ������� �������� �/��� ������������ �������.*/
	float _fo;
	/** �������� ��������.*/
	int _Br;
	/** ������ ���������.*/
	float _T;
	/** ������� ������������� �������� �/��� ������������ �������.*/
	float _Fs;
	/** ������� ������� ��� ������������ �������.*/
	float _dopler_freq;
	/** ������� ������� ��� �������� �������.*/
	float _dopler_freq_for_ref_sig;
	/** ��������� ������/���(��).*/
	float _d;
	/** ��� ���������.*/
	/** ��� ������������� ���� ��������� K = 3, ���������� ��������� �� 3
	* ����� �������� �������� � �������� BPSK_Modulation � MSK_Modulation.*/
	int _K;
	/** ������������ ������� ������.*/
	std::vector <bool> vec_refsig;
	/** ������������ ����������� ������.*/
	std::vector <bool> vec_explrsig;
	/** ������ �������� �������� �������.*/
	std::vector<cufftComplex> Vec_RefSig;
	/** ������ �������� �������� ������� (����������� �� 2^n).*/
	std::vector<cufftComplex> Vec_RefSigEx;
	/** ������ �������� ������������ �������.*/
	std::vector<cufftComplex> Vec_ExplrSig;
	/** ������ �������� ������������ ������� (����������� �� 2^n).*/
	std::vector<cufftComplex> Vec_ExplrSigEx;
	/** ������ �������� ��.*/
	std::vector<std::vector<float>> vec_amb_func;
	/** ������ �������� �� (�������� ������ ���������).*/
	std::vector<float> Vec_CAF;


public:
	/** ����������� �� ���������.*/
	Algorithm();

	/** ����������� � �����������.
	* @param N1 - ���������� ��� � ������� �������.
	* @param N2 - ���������� ��� � ����������� �������.
	* @param time_delay - ��������� �������� �������� �������.
	* @param fo - ������� ������� �������� �/��� ������������ �������.
	* @param Br - �������� ��������.
	* @param T - ������ ���������.
	* @param Fs - ������� ������������� �������� �/��� ������������ �������.
	* @param dopler_freq - ������� ������� ��� ������������ �������.
	* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
	* @param d - ��������� ������/���(��).
	* @param K - ��� ���������.
	*/
	Algorithm(int N1, int N2, int time_delay,
		float fo, int Br, float T, float Fs,
		float dopler_freq, float dopler_freq_for_ref_sig, float d, int K);

	/** ����������.*/
	~Algorithm();

	/** ������ ��������� ��������.*/
	virtual void SetParams(int N1, int N2, int time_delay,
		float fo, int Br, float T, float Fs,
		float dopler_freq, float dopler_freq_for_ref_sig, float d, int K) = 0;

	/** �������� �������� ��������� ��������.
	* @return _time_delay - �������� ���.
	*/
	int Get_Delay();

	/** ����� ��������� ������ ����� � x, ������ ������� ������.
	* @param x - ��������, ������� ���������� ��������� �� ������� ������.
	* @return - ��������, ������ ������� ������.
	*/
	int pow_2(int x);

	/** ������������� ������������ �������(������� � �����������).
	* ������� ������������ ����� ������������������ ���(0 � 1).
	* @param delay - ��������� �������� �������� �������.
	*/
	void CreateBitSignals(int delay);

	/** �������� ��� �� �������.
	* @param &Vec_Buf - ������, �� ������� ����������� ���.
	*/
	void Noise(std::vector <float> & Vec_Buf);

	/** �������� ��������� BPSK.
	* @param vec_ref - ������� ������.
	* @param vec_explr - ����������� ������.
	*/
	void ModulationBPSK(std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr);

	/** �������� ��������� MSK.
	* @param vec_ref - ������� ������.
	* @param vec_explr - ����������� ������.
	*/
	void ModulationMSK(std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr);

	/** ������� ��� ���������.
	* @param BPSK_or_MSK - ���� BPSK_or_MSK = true, �� BPSK - ���������, ����� MSK - ���������.
	* @param vec_ref - ������� ������.
	* @param vec_explr - ����������� ������.
	*/
	void SelectModulation(bool BPSK_or_MSK, std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr);

	/** ���������� ���.
	* @param *data - ������ ��� ������� ���������� ���.
	* @param n - ���������� �������� � �������.
	* @param is - ���� is = -1 - ������, ���� is = 1 - ��������.
	*/
	void fourier(cufftComplex *data, int n, int is);

	/** ��������� ���.*/
	virtual void CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
		const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr) = 0;
};
