#pragma once

#include "Algorithm.h"

class CUDAMatrix : public Algorithm
{
private:
	/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
	void signal_mult(int numBlocks, int blockSize, cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes);

	/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
	void abs_fft(int numBlocks, int blockSize, cufftComplex * pDevSigDec, float * pDevAbsFFT);

	/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
	void abs_fft_matrix(int numBlocks, int blockSize, cufftComplex * d_arrRes, float * pDevResAbsFFT);

	/** �������, � ������� ����������� �� ���������� ���� kernel_mul_matrix.*/
	void mul_matrix(cufftComplex *A, cufftComplex *B, int width_A, int height_A, int width_B, cufftComplex *C);

	/** �������, � ������� ����������� �� ���������� ���� kernel_matrix_transform.*/
	void matrix_transform(cufftComplex *M, cufftComplex *N, int width_M, int height_M, int width_N, int height_N);


public:
	/** ����������� �� ���������.*/
	CUDAMatrix();

	/** ����������.*/
	~CUDAMatrix();

	/** ������ ��������� ��������.
	* @param N1 - ���������� ��� � ������� �������.
	* @param N2 - ���������� ��� � ����������� �������.
	* @param time_delay - ��������� �������� �������� �������.
	* @param fo - ������� ������� �������� �/��� ������������ �������.
	* @param Br - �������� ��������.
	* @param T - ������ ���������.
	* @param Fs - ������� ������������� �������� �/��� ������������ �������.
	* @param dopler_freq - ������� ������� ��� ������������ �������.
	* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
	* @param d - ��������� ������/���(��).
	* @param K - ��� ���������.
	*/
	void SetParams(int N1, int N2, int time_delay,
		float fo, int Br, float T, float Fs,
		float dopler_freq, float dopler_freq_for_ref_sig, float d, int K) override;

	/** ��������� ���.
	* @param delay - �������� ��������� �������� �������� �������.
	* @param max - ������������ �������� ��.
	* @param QA - �������� ��������.
	* @param argc - ����� ���������� ��������� ������.
	* @param argv - ������ ���������� ��������� ������.
	* @param K - ��� ���������.
	* @param vec_ref - ������� ������.
	* @param vec_explr - ����������� ������.
	*/
	void CAF_Calculation(int & delay, float & max, float & QA,int argc, char* argv[], int K,
		const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr) override;

	/** ��������� ��� � �������� [-K, K] ������ ���.
	* @param delay - ��� �������� �������.
	* @param K - ��� ���������.
	* @param vec_ref - ������� ������.
	* @param vec_exple - ����������� ������.
	*/
	void CAFCalcPrecision(int & delay, int K, const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr);
};
