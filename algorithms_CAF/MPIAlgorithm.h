#pragma once

#include "Algorithm.h"

class MPIAlgorithm : public Algorithm
{
public:
	/** ����������� �� ���������.*/
	MPIAlgorithm();

	/** ����������.*/
	~MPIAlgorithm();

	/** ������ ��������� ��������.
	* @param N1 - ���������� ��� � ������� �������.
	* @param N2 - ���������� ��� � ����������� �������.
	* @param time_delay - ��������� �������� �������� �������.
	* @param fo - ������� ������� �������� �/��� ������������ �������.
	* @param Br - �������� ��������.
	* @param T - ������ ���������.
	* @param Fs - ������� ������������� �������� �/��� ������������ �������.
	* @param dopler_freq - ������� ������� ��� ������������ �������.
	* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
	* @param d - ��������� ������/���(��).
	* @param K - ��� ���������.
	*/
	void SetParams(int N1, int N2, int time_delay,
		float fo, int Br, float T, float Fs,
		float dopler_freq, float dopler_freq_for_ref_sig, float d, int K) override;
	
	/** ��������� ���.
	* @param delay - �������� ��������� �������� �������� �������.
	* @param max - ������������ �������� ��.
	* @param QA - �������� ��������.
	* @param argc - ����� ���������� ��������� ������.
	* @param argv - ������ ���������� ��������� ������.
	* @param K - ��� ���������.
	* @param vec_ref - ������� ������.
	* @param vec_explr - ����������� ������.
	*/
	void CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
		const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr) override;
};

