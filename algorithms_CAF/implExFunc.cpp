#include "stdafx.h"
#include <lib/ExportMacro.h>
#include "AlgUser.h"
#include "SeqAlgorithm.h"
#include "MPIAlgorithm.h"
#include "CUDAAlgorithm.h"
#include "CUDAMatrix.h"
#include "CudaMatrixEx.h"

/** Сгенерировать опорный и исследуемый сигналы.
* @param AlgNum - параметр, отвечающий за алгоритм вычисления ВФН:
* AlgNum = 1 - последовательный алгоритм вычисления ВФН.
* AlgNum = 2 - параллельный алгоритм вычисления ВФН с помощью MPI.
* AlgNum = 3 - параллельный алгоритм вычисления ВФН с помощью CUDA.
* @param N1 - количество бит в опорном сигнале.
* @param N2 - количество бит в исследуемом сигнале.
* @param time_delay - временная задержка опорного сигнала.
* @param fo - несущая частота опорного и/или исследуемого сигнала.
* @param Br - скорость передачи.
* @param T - период модуляции (передается 1.0f / Br).
* @param Fs - частота дискретизации опорного и/или исследуемого сигнала (МГц).
* @param dopler_freq - частота Доплера для исследуемого сигнала.
* @param dopler_freq_for_ref_sig - частота Доплера для опорного сигнала.
* @param d - отношение сигнал/шум(Дб).
* @param K - шаг децимации.
* @param BPSK_or_MSK - если BPSK_or_MSK = true, то BPSK - модуляция, иначе MSK - модуляция.
* @param vec_ref - опорный сигнал (выходной параметр).
* @param vec_explr - исследуемый сигнал (выходной параметр).
*/
void generate_signals(int AlgNum, int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs, float dopler_freq, float dopler_freq_for_ref_sig, float d, int K, bool BPSK_or_MSK,
	std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr)
{
	AlgUser alg;
	switch (AlgNum)
	{
	case 1:
	{
		SeqAlgorithm seq_alg;
		alg.gen_sig(&seq_alg, N1, N2, time_delay, fo, Br, T, Fs, dopler_freq, dopler_freq_for_ref_sig, d, K, BPSK_or_MSK, vec_ref, vec_explr);
		break;
	}
	case 2:
	{
		MPIAlgorithm mpi_alg;
		alg.gen_sig(&mpi_alg, N1, N2, time_delay, fo, Br, T, Fs, dopler_freq, dopler_freq_for_ref_sig, d, K, BPSK_or_MSK, vec_ref, vec_explr);
		break;
	}
	case 3:
	{
		CUDAAlgorithm cuda_alg;
		alg.gen_sig(&cuda_alg, N1, N2, time_delay, fo, Br, T, Fs, dopler_freq, dopler_freq_for_ref_sig, d, K, BPSK_or_MSK, vec_ref, vec_explr);
		break;
	}
	case 4:
	{
		CUDAMatrix cuda_mtrx;
		alg.gen_sig(&cuda_mtrx, N1, N2, time_delay, fo, Br, T, Fs, dopler_freq, dopler_freq_for_ref_sig, d, K, BPSK_or_MSK, vec_ref, vec_explr);
		break;
	}
	case 5:
	{
		CudaMatrixEx cuda_mtrx_ex;
		alg.gen_sig(&cuda_mtrx_ex, N1, N2, time_delay, fo, Br, T, Fs, dopler_freq, dopler_freq_for_ref_sig, d, K, BPSK_or_MSK, vec_ref, vec_explr);
		break;
	}
	default:
		break;
	}
}

/** Вычислить ВФН.
* @param AlgNum - параметр, отвечающий за алгоритм вычисления ВФН:
* AlgNum = 1 - последовательный алгоритм вычисления ВФН.
* AlgNum = 2 - параллельный алгоритм вычисления ВФН с помощью MPI.
* AlgNum = 3 - параллельный алгоритм вычисления ВФН с помощью CUDA.
* @param delay - ВВЗ, соответствующая максимуму ВФН (выходной параметр).
* @param max - максимум ВФН (выходной параметр).
* @param argc - число параметров командной строки.
* @param argv - указатель на массив параметров командной строки.
* @param K - шаг децимации.
* @param QA - критерий качества ВФН.
* @param vec_ref - опорный сигнал.
* @param vec_explr - исследуемый сигнал.
*/
void calculation(int AlgNum, int & delay, float & max, int argc, char* argv[], int K, float & QA,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	AlgUser alg;
	switch (AlgNum)
	{
	case 1:
	{
		SeqAlgorithm seq_alg;
		alg.calc(&seq_alg, delay, max, QA, argc, argv, K, vec_ref, vec_explr);
		break;
	}
	case 2:
	{
		MPIAlgorithm mpi_alg;
		alg.calc(&mpi_alg, delay, max, QA, argc, argv, K, vec_ref, vec_explr);
		break;
	}
	case 3:
	{
		CUDAAlgorithm cuda_alg;
		alg.calc(&cuda_alg, delay, max, QA, argc, argv, K, vec_ref, vec_explr);
		break;
	}
	case 4:
	{
		CUDAMatrix cuda_mtrx;
		alg.calc(&cuda_mtrx, delay, max, QA, argc, argv, K, vec_ref, vec_explr);
		break;
	}
	case 5:
	{
		CudaMatrixEx cuda_mtrx_ex;
		alg.calc(&cuda_mtrx_ex, delay, max, QA, argc, argv, K, vec_ref, vec_explr);
		break;
	}
	default:
		break;
	}
}

