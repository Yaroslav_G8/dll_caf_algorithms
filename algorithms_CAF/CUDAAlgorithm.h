#pragma once

#include "Algorithm.h"

class CUDAAlgorithm : public Algorithm
{
private:
	/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
	void signal_mult(int numBlocks, int blockSize,
		cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes);
	/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
	void abs_fft(int numBlocks, int blockSize,
		cufftComplex * pDevSigDec, float * pDevAbsFFT);
public:
	/** ����������� �� ���������.*/
	CUDAAlgorithm();
	/** ����������.*/
	~CUDAAlgorithm();
	/** ������ ��������� ��������.*/
	void SetParams(int N1, int N2, int time_delay,
		float fo, int Br, float T, float Fs,
		float dopler_freq, float dopler_freq_for_ref_sig, float d, int K) override;
	/** ��������� ���.*/
	void CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
		const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr) override;
};

