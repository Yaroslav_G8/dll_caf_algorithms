#include "stdafx.h"
#include "CUDAMatrix.h"
#include "cu_func.h"
#include <iostream>
#include <cublas.h>
#include <initializer_list>

#define _CRT_SECURE_NO_WARNINGS

/** ������ ����� �� GPU.*/
#define BLOCK_SIZE 512

/** ����������� �� ���������.*/
CUDAMatrix::CUDAMatrix()
{
}

/** ����������.*/
CUDAMatrix::~CUDAMatrix()
{
}

/** ������ ��������� ��������.
* @param N1 - ���������� ��� � ������� �������.
* @param N2 - ���������� ��� � ����������� �������.
* @param time_delay - ��������� �������� �������� �������.
* @param fo - ������� ������� �������� �/��� ������������ �������.
* @param Br - �������� ��������.
* @param T - ������ ���������.
* @param Fs - ������� ������������� �������� �/��� ������������ �������.
* @param dopler_freq - ������� ������� ��� ������������ �������.
* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
* @param d - ��������� ������/���(��).
* @param K - ��� ���������.
*/
void CUDAMatrix::SetParams(int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs,
	float dopler_freq, float dopler_freq_for_ref_sig, float d, int K)
{
	_N1 = N1;
	_N2 = N2;
	_time_delay = time_delay;
	_fo = fo;
	_Br = Br;
	_T = T;
	_Fs = Fs;
	_dopler_freq = dopler_freq;
	_dopler_freq_for_ref_sig = dopler_freq_for_ref_sig;
	_d = d;
	_K = K;
}

/** ��������� ��� � �������� [-K, K] ������ ���.
* @param delay - ��� �������� �������.
* @param K - ��� ���������.
* @param vec_ref - ������� ������.
* @param vec_exple - ����������� ������.
*/
void CUDAMatrix::CAFCalcPrecision(int & delay, int K, const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	/** ������ ������� ������������ �������.*/
	size_t size_vec_explr_sig = vec_explr.size();
	/** ������ ������� �������� �������.*/
	size_t size_vec_ref_sig = vec_ref.size();
	/** ������ ������� �������� ���.*/
	size_t size_CAF = 2 * K + 1;

	/** ��������� ������ ��� ������� ���.*/
	vec_amb_func.resize(size_CAF);
	for (size_t i = 0; i < size_CAF; i++)
	{
		vec_amb_func[i].resize(size_vec_ref_sig / K);
	}

	/** �������� �� ������� ������� vec_amb_func.*/
	float maximum = 0.0f;
	/** ������ ���������� ���.*/
	std::vector<float> Vec_CAF_Buf;
	Vec_CAF_Buf.resize(size_CAF);

	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	if (!prop.canMapHostMemory)
		exit(0);
	cudaSetDeviceFlags(cudaDeviceMapHost);

	/** ������ ����� ��� ���������� ������������ �������� ��������.*/
	int blockSizeSigMult = 512;
	/** ����� ������ ��� ���������� ������������ �������� ��������.*/
	int numBlocksSigMult = (int)(size_vec_ref_sig) / blockSizeSigMult;

	/** ������� ������ �� GPU � host.*/
	cufftComplex *pHostRefSig, *pDevRefSig;
	cudaHostAlloc((void**)&pHostRefSig, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevRefSig, (void*)pHostRefSig, 0);
	for (int it = 0; it < size_vec_ref_sig; it++)
	{
		pHostRefSig[it].x = vec_ref[it].x;
		pHostRefSig[it].y = vec_ref[it].y;
	}

	/** ����������� ������ �� GPU � host.*/
	cufftComplex *pHostExplrSig, *pDevExplrSig;
	cudaHostAlloc((void**)&pHostExplrSig, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevExplrSig, (void*)pHostExplrSig, 0);
	
	/** �������, ���������� � ���������� ������������ ��������, �� GPU � host.*/
	cufftComplex *pHostRes, *pDevRes;
	cudaHostAlloc((void**)&pHostRes, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevRes, (void*)pHostRes, 0);

	/** ������� ����� ��������� ��������.*/
	cufftComplex *host_arr_decim, *dev_arr_decim;
	cudaHostAlloc((void**)&host_arr_decim, sizeof(cufftComplex) * size_vec_ref_sig / K, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&dev_arr_decim, (void*)host_arr_decim, 0);

	/** �������� ������������ ��� ���.*/
	cufftHandle plan;
	if (cufftPlan1d(&plan, (int)size_vec_ref_sig / K, CUFFT_C2C, BATCH) != CUFFT_SUCCESS)
	{
		std::cout << "CUFFT error: Plan creation failed" << std::endl;
	}

	/** ������ ����� ��� ���������� ������ ��� ����� ������������ �������� ��������.*/
	int blockSizeAbsFFT = 512;
	/** ����� ������ ��� ���������� ������ ��� ����� ������������ �������� ��������.*/
	int numBlocksAbsFFT = (int)(size_vec_ref_sig) / K / blockSizeAbsFFT;

	/** ������ ��� �� GPU � host.*/
	float *pDevResAbsFFT, *pHostResAbsFFT;
	cudaHostAlloc((void**)&pHostResAbsFFT, sizeof(float) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevResAbsFFT, (void*)pHostResAbsFFT, 0);
	int idx_K = -K;
	/** ���������� ��.*/
	for (size_t i = 0; i < size_CAF; i++)
	{
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			pHostExplrSig[j].x = vec_explr[j + idx_K + delay].x;
			pHostExplrSig[j].y = vec_explr[j + idx_K + delay].y;
		}

		signal_mult(numBlocksSigMult, blockSizeSigMult, pDevRefSig, pDevExplrSig, pDevRes);
		cudaDeviceSynchronize();

		int k;
		int it = 0;
		size_t j = 0;
		memset(host_arr_decim, 0, sizeof(cufftComplex) * size_vec_ref_sig / K);
		while (j < size_vec_ref_sig)
		{
			k = 0;
			while (k < K)
			{
				host_arr_decim[it].x += pHostRes[j].x;
				host_arr_decim[it].y += pHostRes[j].y;
				k++; j++;
			}
			it++;
		}

		cufftExecC2C(plan, dev_arr_decim, dev_arr_decim, CUFFT_FORWARD);
		cudaDeviceSynchronize();

		abs_fft(numBlocksAbsFFT, blockSizeAbsFFT, dev_arr_decim, pDevResAbsFFT);
		cudaDeviceSynchronize();

		for (int it = 0; it < size_vec_ref_sig / K; it++)
		{
			vec_amb_func[i][it] = pHostResAbsFFT[it];
		}

		maximum = 0;
		for (size_t k = 0; k < vec_amb_func[i].size(); k++)
		{
			if (maximum < vec_amb_func[i][k])
				maximum = vec_amb_func[i][k];
		}
		Vec_CAF_Buf[i] = maximum;
		++idx_K;
	}

	cudaFreeHost(pHostRefSig);
	cudaFreeHost(pHostExplrSig);
	cudaFreeHost(pHostRes);

	cudaFreeHost(host_arr_decim);
	cufftDestroy(plan);

	cudaFreeHost(pHostResAbsFFT);

	float Max = 0;
	idx_K = -K + delay;
	for (size_t it = 0; it < size_CAF; it++)
	{
		if (Max < Vec_CAF_Buf[it])
		{
			Max = Vec_CAF_Buf[it];
			delay = idx_K;
		}
		++idx_K;
	}
}

/** ��������� ���.
* @param delay - �������� ��������� �������� �������� �������.
* @param max - ������������ �������� ��.
* @param QA - �������� ��������.
* @param argc - ����� ���������� ��������� ������.
* @param argv - ������ ���������� ��������� ������.
* @param K - ��� ���������.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
void CUDAMatrix::CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	/** ������ ������� ������������ �������.*/
	size_t size_vec_explr_sig = vec_explr.size();
	/** ������ ������� �������� �������.*/
	size_t size_vec_ref_sig = vec_ref.size();
	/** ������ ������� �������� ���.*/
	size_t size_CAF = (size_vec_explr_sig - size_vec_ref_sig) / K * size_vec_ref_sig / K;

	/** ����������� ������ �� GPU.*/
	cufftComplex *d_A;
	cudaMalloc((void**)&d_A, size_vec_explr_sig * sizeof(cufftComplex));
	/** ������� ������ �� GPU.*/
	cufftComplex *d_B;
	cudaMalloc((void**)&d_B, size_vec_ref_sig * sizeof(cufftComplex));

	/** ����������� ������ �� host.*/
	cufftComplex *h_A = (cufftComplex *)malloc(size_vec_explr_sig * sizeof(cufftComplex));
	/** ������������� ������������ ������� � "���������" ����.*/
	for (size_t i = 0; i < size_vec_explr_sig; ++i)
	{
		h_A[i] = vec_explr[i];
	}

	/** ������� ������ �� host.*/
	cufftComplex *h_B = (cufftComplex *)malloc(size_vec_ref_sig * sizeof(cufftComplex));
	int it_B = 0;
	/** ������������� �������� ������� � "���������" ����.*/
	for (int i = 0; i < K; ++i)
	{
		for (int j = 0; j < size_vec_ref_sig / K; ++j)
		{
			h_B[it_B] = vec_ref[i + j * K];
			++it_B;
		}
	}
	
	/** ����������� �������� � ����� �� GPU.*/
	cudaMemcpy(d_A, h_A, size_vec_explr_sig * sizeof(cufftComplex), cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, h_B, size_vec_ref_sig * sizeof(cufftComplex), cudaMemcpyHostToDevice);

	/** �������������� ������� �� host.*/
	size_t size_C = (size_t)size_vec_explr_sig / K * size_vec_ref_sig / K;
	cufftComplex *h_C = (cufftComplex *)malloc(size_C * sizeof(cufftComplex));
	/** �������������� ������� �� GPU.*/
	cufftComplex *d_C;
	cudaMalloc((void**)&d_C, size_C * sizeof(cufftComplex));

	mul_matrix(d_A, d_B, (int)K, (int)size_vec_explr_sig / K, (int)size_vec_ref_sig / K, d_C);
	cudaDeviceSynchronize();

	cudaMemcpy(h_C, d_C, size_C * sizeof(cufftComplex), cudaMemcpyDeviceToHost);

	/** ��������������� �������������� ������� �� host.*/
	cufftComplex *h_C_trans = (cufftComplex *)malloc(size_CAF * sizeof(cufftComplex));
	/** ��������������� �������������� ������� �� GPU.*/
	cufftComplex * d_C_trans;
	cudaMalloc((void**)&d_C_trans, size_CAF * sizeof(cufftComplex));

	matrix_transform(d_C, d_C_trans, (int)size_vec_ref_sig / K, (int)size_vec_explr_sig / K,
		(int)size_vec_ref_sig / K, (int)(size_vec_explr_sig - size_vec_ref_sig) / K);
	cudaDeviceSynchronize();

	cudaMemcpy(h_C_trans, d_C_trans, size_CAF * sizeof(cufftComplex), cudaMemcpyDeviceToHost);
	
	/** �������� ������������ ��� ���.*/
	cufftHandle plan;
	cufftResult res = cufftPlan1d(&plan, (int)size_vec_ref_sig / K, CUFFT_C2C, (int)(size_vec_explr_sig - size_vec_ref_sig) / K);
	if (res != CUFFT_SUCCESS)
	{
		std::cout << "CUFFT error: Plan creation failed" << std::endl;
	}

	cufftExecC2C(plan, d_C_trans, d_C_trans, CUFFT_FORWARD);
	cudaDeviceSynchronize();

	/** ����� ������ ��� ���������� ������ ��� ����� ������������ �������� ��������.*/
	int numBlocksAbsFFT = (int)(size_CAF) / BLOCK_SIZE;

	/** ������ ��� �� host.*/
	float * pHostResAbsFFT = (float *)malloc(size_CAF * sizeof(float));
	/** ������ ��� �� GPU.*/
	float *pDevResAbsFFT;
	cudaMalloc((void**)&pDevResAbsFFT, (size_t)size_CAF * sizeof(float));

	abs_fft_matrix(numBlocksAbsFFT, BLOCK_SIZE, d_C_trans, pDevResAbsFFT);
	cudaDeviceSynchronize();

	cudaMemcpy(pHostResAbsFFT, pDevResAbsFFT, size_CAF * sizeof(float), cudaMemcpyDeviceToHost);

	size_t height = (size_vec_explr_sig - size_vec_ref_sig) / K;
	size_t width = size_vec_ref_sig / K;
	int row = 0;
	int col = 0;
	max = 0.0f; delay = 0; QA = 0.0f;
	size_t index = 0;
	for (size_t i = 0; i < height; ++i)
	{
		for (size_t j = 0; j < width; ++j)
		{
			if (max < pHostResAbsFFT[index])
			{
				max = pHostResAbsFFT[index];
				row = (int)i;
				col = (int)j;
			}
			++index;
		}
	}

	/** �������������� ��������.*/
	float sum_avrg = 0.0f;
	for (size_t i = 0; i < size_CAF; ++i)
	{
		pHostResAbsFFT[i] /= max;
		sum_avrg += pHostResAbsFFT[i];
	}
	sum_avrg /= size_CAF;

	/** ���������.*/
	float D = 0.0f;
	for (size_t i = 0; i < size_CAF; ++i)
	{
		D += (pHostResAbsFFT[i] - sum_avrg) * (pHostResAbsFFT[i] - sum_avrg);
	}
	max = 0.0f;
	for (size_t j = 0; j < size_CAF; ++j)
	{
		if (max < pHostResAbsFFT[j])
		{
			max = pHostResAbsFFT[j];
		}
	}
	QA = max / sqrt(D);
	delay = row * K;
	CAFCalcPrecision(delay, K, vec_ref, vec_explr);

	/** ������� ������.*/
	free(pHostResAbsFFT);
	cudaFree(pDevResAbsFFT);
	free(h_C_trans);
	cudaFree(d_C_trans);
	free(h_C);
	cudaFree(d_C);
	free(h_B);
	cudaFree(d_B);
	free(h_A);
	cudaFree(d_A);
	cufftDestroy(plan);
}