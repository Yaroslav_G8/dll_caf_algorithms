#include "stdafx.h"
#include "CUDAAlgorithm.h"
#include "cu_func.h"
#include <iostream>

/** ����������� �� ���������.*/
CUDAAlgorithm::CUDAAlgorithm()
{
}

/** ����������.*/
CUDAAlgorithm::~CUDAAlgorithm()
{
}

/** ������ ��������� ��������.*/
void CUDAAlgorithm::SetParams(int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs,
	float dopler_freq, float dopler_freq_for_ref_sig, float d, int K)
{
	_N1 = N1;
	_N2 = N2;
	_time_delay = time_delay;
	_fo = fo;
	_Br = Br;
	_T = T;
	_Fs = Fs;
	_dopler_freq = dopler_freq;
	_dopler_freq_for_ref_sig = dopler_freq_for_ref_sig;
	_d = d;
	_K = K;
}

/** ���������� ���.*/
void CUDAAlgorithm::CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	/** ������ ������� ������������ �������.*/
	size_t size_vec_explr_sig = vec_explr.size();
	/** ������ ������� �������� �������.*/
	size_t size_vec_ref_sig = vec_ref.size();
	/** ������ ������� �������� ���.*/
	size_t size_CAF = size_vec_explr_sig - size_vec_ref_sig;

	/** ��������� ������ ��� ������� ���.*/
	vec_amb_func.resize(size_CAF);
	for (size_t i = 0; i < size_CAF; i++)
	{
		vec_amb_func[i].resize(size_vec_ref_sig / K);
	}

	/** �������� �� ������� ������� vec_amb_func.*/
	float maximum;
	/** ������ ���������� ���.*/
	std::vector<float> Vec_CAF_Buf;
	Vec_CAF_Buf.resize(size_CAF);

	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	if (!prop.canMapHostMemory)
		exit(0);
	cudaSetDeviceFlags(cudaDeviceMapHost);

	/** ������ ����� ��� ���������� ������������ �������� ��������.*/
	const unsigned int blockSizeSigMult = 512;
	/** ����� ������ ��� ���������� ������������ �������� ��������.*/
	const unsigned int numBlocksSigMult = (unsigned int)size_vec_ref_sig / blockSizeSigMult;
	/** ���������� ����� ��� ���������� ������������ �������� ��������.*/
	const unsigned int numItemsSigMult = blockSizeSigMult * numBlocksSigMult;

	/** ��������� ������ �� GPU ��� �������� � ������������ ��������.*/
	cufftComplex *pHostRefSig, *pDevRefSig;
	cudaHostAlloc((void**)&pHostRefSig, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevRefSig, (void*)pHostRefSig, 0);
	for (int it = 0; it < size_vec_ref_sig; it++)
	{
		pHostRefSig[it].x = vec_ref[it].x;
		pHostRefSig[it].y = vec_ref[it].y;
	}
	cufftComplex *pHostExplrSig, *pDevExplrSig;
	cudaHostAlloc((void**)&pHostExplrSig, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevExplrSig, (void*)pHostExplrSig, 0);
	cufftComplex *pHostRes, *pDevRes;
	cudaHostAlloc((void**)&pHostRes, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevRes, (void*)pHostRes, 0);
	/** ��������� ������ �� GPU ��� ���������� ���.*/
	cufftComplex *host_arr_decim, *dev_arr_decim;
	cudaHostAlloc((void**)&host_arr_decim, sizeof(cufftComplex) * size_vec_ref_sig / K, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&dev_arr_decim, (void*)host_arr_decim, 0);
	/** �������� ������������ ��� ���.*/
	cufftHandle plan;
	if (cufftPlan1d(&plan, (int)size_vec_ref_sig / K, CUFFT_C2C, BATCH) != CUFFT_SUCCESS)
	{
		std::cout << "CUFFT error: Plan creation failed" << std::endl;
	}

	/** ������ ����� ��� ���������� ������ ��� ����� ������������ �������� ��������.*/
	const unsigned int blockSizeAbsFFT = 512;
	/** ����� ������ ��� ���������� ������ ��� ����� ������������ �������� ��������.*/
	const unsigned int numBlocksAbsFFT = (unsigned int)size_vec_ref_sig / K / blockSizeAbsFFT;
	/** ���������� ����� ��� ���������� ������ ��� ����� ������������ �������� ��������.*/
	const unsigned int numItemsAbsFFT = blockSizeAbsFFT * numBlocksAbsFFT;

	/** ��������� ������ �� GPU ��� ���������� ������ ���.*/
	float *pDevResAbsFFT, *pHostResAbsFFT;
	cudaHostAlloc((void**)&pHostResAbsFFT, sizeof(float) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevResAbsFFT, (void*)pHostResAbsFFT, 0);
	/** ���������� ��.*/
	for (size_t i = 0; i < size_CAF; i++)
	{
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			pHostExplrSig[j].x = vec_explr[j + i].x;
			pHostExplrSig[j].y = vec_explr[j + i].y;
		}

		signal_mult(numBlocksSigMult, blockSizeSigMult, pDevRefSig, pDevExplrSig, pDevRes);

		cudaThreadSynchronize();

		int k;
		int it = 0;
		size_t j = 0;
		memset(host_arr_decim, 0, sizeof(cufftComplex) * size_vec_ref_sig / K);
		while (j < size_vec_ref_sig)
		{
			k = 0;
			while (k < K)
			{
				host_arr_decim[it].x += pHostRes[j].x;
				host_arr_decim[it].y += pHostRes[j].y;
				k++; j++;
			}
			it++;
		}

		cufftExecC2C(plan, dev_arr_decim, dev_arr_decim, CUFFT_FORWARD);
		cudaThreadSynchronize();

		abs_fft(numBlocksAbsFFT, blockSizeAbsFFT, dev_arr_decim, pDevResAbsFFT);

		cudaThreadSynchronize();

		for (int it = 0; it < size_vec_ref_sig / K; it++)
		{
			vec_amb_func[i][it] = pHostResAbsFFT[it];
		}

		maximum = 0;
		for (size_t k = 0; k < vec_amb_func[i].size(); k++)
		{
			if (maximum < vec_amb_func[i][k])
				maximum = vec_amb_func[i][k];
		}
		Vec_CAF_Buf[i] = maximum;
	}

	cudaFreeHost(pHostRefSig);
	cudaFreeHost(pHostExplrSig);
	cudaFreeHost(pHostRes);

	cudaFreeHost(host_arr_decim);
	cufftDestroy(plan);

	cudaFreeHost(pHostResAbsFFT);

	max = 0;
	for (size_t it = 0; it < size_CAF; it++)
	{
		if (max < Vec_CAF_Buf[it])
		{
			max = Vec_CAF_Buf[it];
			delay = int(it);
		}
	}
	std::for_each(Vec_CAF_Buf.begin(), Vec_CAF_Buf.end(), [max](float & x) { x /= max; });
	/** �������������� ��������.*/
	float sum_avrg = 0;
	for (size_t i = 0; i < size_CAF; ++i)
	{
		sum_avrg += Vec_CAF_Buf[i];
	}
	sum_avrg /= size_CAF;
	/** ���������.*/
	float D = 0;
	for (size_t i = 0; i < size_CAF; ++i)
	{
		D += (Vec_CAF_Buf[i] - sum_avrg) * (Vec_CAF_Buf[i] - sum_avrg);
	}
	max = 0;
	for (size_t j = 0; j < size_CAF; ++j)
	{
		if (max < Vec_CAF_Buf[j])
		{
			max = Vec_CAF_Buf[j];
		}
	}
	QA = max / sqrt(D);
}