#pragma once
#include "Algorithm.h"

class AlgUser
{
public:
	/** ����������� �� ���������.*/
	AlgUser();
	void gen_sig(Algorithm * alg, int N1, int N2, int time_delay,
		float fo, int Br, float T, float Fs, float dopler_freq, float dopler_freq_for_ref_sig, float d, int K, bool BPSK_or_MSK,
		std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr);
	/** ������������ ���������� �������� ���������� ���.*/
	void calc(Algorithm * alg, int & delay, float & max, float & QA, int argc, char* argv[], int K,
		const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr);
	/** ����������.*/
	~AlgUser();
};

