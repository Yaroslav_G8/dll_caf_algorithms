#include "stdafx.h"
#include "Algorithm.h"

/** ����������� �� ���������.*/
Algorithm::Algorithm()
{
	this->_N1 = 0;
	this->_N2 = 0;
	this->_time_delay = 0;
	this->_fo = 0.0f;
	this->_Br = 0;
	this->_T = 0.0f;
	this->_Fs = 0.0f;
	this->_dopler_freq = 0.0f;
	this->_dopler_freq_for_ref_sig = 0.0f;
	this->_d = 0.0f;
	this->_K = 0;
}

/** ����������� � �����������.
* @param N1 - ���������� ��� � ������� �������.
* @param N2 - ���������� ��� � ����������� �������.
* @param time_delay - ��������� �������� �������� �������.
* @param fo - ������� ������� �������� �/��� ������������ �������.
* @param Br - �������� ��������.
* @param T - ������ ���������.
* @param Fs - ������� ������������� �������� �/��� ������������ �������.
* @param dopler_freq - ������� ������� ��� ������������ �������.
* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
* @param d - ��������� ������/���(��).
* @param K - ��� ���������.
*/
Algorithm::Algorithm(
	int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs,
	float dopler_freq, float dopler_freq_for_ref_sig, float d, int K)
{
	this->_N1 = N1;
	this->_N2 = N2;
	this->_time_delay = time_delay;
	this->_fo = fo;
	this->_Br = Br;
	this->_T = T;
	this->_Fs = Fs;
	this->_dopler_freq = dopler_freq;
	this->_dopler_freq_for_ref_sig = dopler_freq_for_ref_sig;
	this->_d = d;
	this->_K = K;
}

/** ����������.*/
Algorithm::~Algorithm()
{
}

/** �������� �������� ��������� ��������.
* @return _time_delay - �������� ���.
*/
int Algorithm::Get_Delay()
{
	return _time_delay;
}

/** ����� ��������� ������ ����� � x, ������ ������� ������.
* @param x - ��������, ������� ���������� ��������� �� ������� ������.
* @return - ��������, ������ ������� ������.
*/
int Algorithm::pow_2(int x)
{
	x--;
	for (int p = 1; p < 32; p <<= 1)
		x |= (x >> p);
	return ++x;
}

/** ������������� ������������ �������(������� � �����������).
* ������� ������������ ����� ������������������ ���(0 � 1).
* @param delay - ��������� �������� �������� �������.
*/
void Algorithm::CreateBitSignals(int delay)
{
	srand(clock());
	vec_refsig.clear();
	vec_refsig.resize(_N1);
	/** ������� ������.*/
	for (int i = 0; i < _N1; i++)
	{
		vec_refsig[i] = rand() % 2;
	}

	vec_explrsig.clear();
	vec_explrsig.resize(_N2);
	/** ����������� ������.*/
	for (int i = 0; i < _N2; i++)
	{
		vec_explrsig[i] = rand() % 2;
	}
	std::copy(vec_refsig.begin(), vec_refsig.end(), vec_explrsig.begin() + delay);
}

/** �������� ��� �� �������.
* @param &Vec_Buf - ������, �� ������� ����������� ���.
*/
void Algorithm::Noise(std::vector <float> & Vec_Buf)
{
	size_t size = Vec_Buf.size();
	// ��������� ����
	float sum_kv_n = 0.0f;
	int M = 0;
	float *arr_n = new float[size];
	for (size_t i = 0; i < size; i++)
	{
		M = rand() % 9 + 12;
		float sum_Qsi = 0.0f;
		for (int j = 0; j < M; j++)
		{
			sum_Qsi += (rand() % 21 - 10) / 10.0f;
		}
		arr_n[i] = sum_Qsi / M;
		sum_kv_n += arr_n[i] * arr_n[i];
	};

	float sum_kv_Sig = 0.0f;
	for (size_t t = 0; t < size; t++)
	{
		sum_kv_Sig += Vec_Buf[t] * Vec_Buf[t];
	};

	float _alpha = 0.0f;
	_alpha = sqrt(pow(10, -_d / 10) * sum_kv_Sig / (sum_kv_n));

	// ��������� ������� ���������� � �����
	for (size_t i = 0; i < size; i++)
	{
		Vec_Buf[i] = Vec_Buf[i] + _alpha * arr_n[i];
	};

	delete[] arr_n;
}

/** �������� ��������� BPSK.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
void Algorithm::ModulationBPSK(std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr)
{
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt_in_bit = (int)(_T * _Fs * 1000000);

	Vec_RefSig.clear();
	Vec_RefSig.resize(_N1 * amnt_pnt_in_bit);

	/** �������� �������� ��������
	* � �������� � ������ ������ ��� ��������� ����.*/
	std::vector<float> Vec_Buf_for_NoiseRe, Vec_Buf_for_NoiseIm;
	Vec_Buf_for_NoiseRe.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);

	Vec_Buf_for_NoiseIm.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	/** ����.*/
	float phase = 0;
	/** ������� ������ � ���������� ��2.*/
	for (int i = 0; i < _N1; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq_for_ref_sig * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].x = (cos(vec_refsig[i] * M_PI) *
				cos(phase) - sin(vec_refsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].y = (cos(vec_refsig[i] * M_PI) *
				sin(phase) + sin(vec_refsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� �������� ������� �� ������� ������.*/
	int num_of_sam = pow_2(_N1 * amnt_pnt_in_bit);
	Vec_RefSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N1 * amnt_pnt_in_bit))
		{
			Vec_RefSigEx[i].x = Vec_RefSig[i].x;
			Vec_RefSigEx[i].y = Vec_RefSig[i].y;
		}
		else
		{
			Vec_RefSigEx[i].x = 0;
			Vec_RefSigEx[i].y = 0;
		}
	}

	/** ��������� ������� �������� ��� ������������ �������.*/
	Vec_Buf_for_NoiseRe.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);
	Vec_Buf_for_NoiseIm.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	Vec_ExplrSig.resize(_N2 * amnt_pnt_in_bit);
	phase = 0;
	// ����������� ������ � ���������� ��2
	for (int i = 0; i < _N2; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].x = (cos(vec_explrsig[i] * M_PI) *
				cos(phase) - sin(vec_explrsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].y = (cos(vec_explrsig[i] * M_PI) *
				sin(phase) + sin(vec_explrsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� ������������ ������� �� ������� ������.*/
	num_of_sam = pow_2(_N2 * amnt_pnt_in_bit);
	Vec_ExplrSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N2 * amnt_pnt_in_bit))
		{
			Vec_ExplrSigEx[i].x = Vec_ExplrSig[i].x;
			Vec_ExplrSigEx[i].y = Vec_ExplrSig[i].y;
		}
		else
		{
			Vec_ExplrSigEx[i].x = 0;
			Vec_ExplrSigEx[i].y = 0;
		}
	}

	vec_ref = Vec_RefSigEx;
	vec_explr = Vec_ExplrSigEx;

	/** ������� �������� ��������.*/
	Vec_Buf_for_NoiseRe.clear();
	Vec_Buf_for_NoiseIm.clear();
}

/** �������� ��������� MSK.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
void Algorithm::ModulationMSK(std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr)
{
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt_in_bit = (int)(_T * _Fs * 1000000);

	Vec_RefSig.clear();
	Vec_RefSig.resize(_N1 * amnt_pnt_in_bit);

	/** �������� �������� ��������
	* � �������� � ������ ������ ��� ��������� ����.*/
	std::vector<float> Vec_Buf_for_NoiseRe, Vec_Buf_for_NoiseIm;
	Vec_Buf_for_NoiseRe.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);

	Vec_Buf_for_NoiseIm.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	/** ����.*/
	float phase = 0;
	/** ������� ������ � ���������� ��2.*/
	for (int i = 0; i < _N1; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq_for_ref_sig * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].x = (cos(vec_refsig[i] * M_PI) *
				cos(phase) - sin(vec_refsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].y = (cos(vec_refsig[i] * M_PI) *
				sin(phase) + sin(vec_refsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� �������� ������� �� ������� ������.*/
	int num_of_sam = pow_2(_N1 * amnt_pnt_in_bit);
	Vec_RefSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N1 * amnt_pnt_in_bit))
		{
			Vec_RefSigEx[i].x = Vec_RefSig[i].x;
			Vec_RefSigEx[i].y = Vec_RefSig[i].y;
		}
		else
		{
			Vec_RefSigEx[i].x = 0;
			Vec_RefSigEx[i].y = 0;
		}
	}

	/** ��������� ������� �������� ��� ������������ �������.*/
	Vec_Buf_for_NoiseRe.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);
	Vec_Buf_for_NoiseIm.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	Vec_ExplrSig.resize(_N2 * amnt_pnt_in_bit);
	phase = 0;
	// ����������� ������ � ���������� ��2
	for (int i = 0; i < _N2; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].x = (cos(vec_explrsig[i] * M_PI) *
				cos(phase) - sin(vec_explrsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].y = (cos(vec_explrsig[i] * M_PI) *
				sin(phase) + sin(vec_explrsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� ������������ ������� �� ������� ������.*/
	num_of_sam = pow_2(_N2 * amnt_pnt_in_bit);
	Vec_ExplrSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N2 * amnt_pnt_in_bit))
		{
			Vec_ExplrSigEx[i].x = Vec_ExplrSig[i].x;
			Vec_ExplrSigEx[i].y = Vec_ExplrSig[i].y;
		}
		else
		{
			Vec_ExplrSigEx[i].x = 0;
			Vec_ExplrSigEx[i].y = 0;
		}
	}

	vec_ref = Vec_RefSigEx;
	vec_explr = Vec_ExplrSigEx;

	/** ������� �������� ��������.*/
	Vec_Buf_for_NoiseRe.clear();
	Vec_Buf_for_NoiseIm.clear();
}

/** ������� ��� ���������.
* @param BPSK_or_MSK - ���� BPSK_or_MSK = true, �� BPSK - ���������, ����� MSK - ���������.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
void Algorithm::SelectModulation(bool BPSK_or_MSK, std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr)
{
	if (BPSK_or_MSK)
		ModulationBPSK(vec_ref, vec_explr);
	else
		ModulationMSK(vec_ref, vec_explr);
}

/** ���������� ���.
* @param *data - ������ ��� ������� ���������� ���.
* @param n - ���������� �������� � �������.
* @param is - ���� is = -1 - ������, ���� is = 1 - ��������.
*/
void Algorithm::fourier(cufftComplex *data, int n, int is)
{
	int i, j, istep;
	int m, mmax;
	float r,
		r1,
		theta,
		w_r,
		w_i,
		temp_r,
		temp_i;
	float pi = 3.1415926f;

	r = pi*is;   // ������ ��� ���������
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].x;
			temp_i = data[j].y;
			data[j].x = data[i].x;
			data[j].y = data[i].y;
			data[i].x = temp_r;
			data[i].y = temp_i;
		}
		m = n >> 1;
		while (j >= m)
		{
			j -= m; m = (m + 1) / 2;
		}
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (float)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1*m;
			w_r = (float)cos((float)theta);
			w_i = (float)sin((float)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r*data[j].x - w_i*data[j].y;
				temp_i = w_r*data[j].y + w_i*data[j].x;
				data[j].x = data[i].x - temp_r;
				data[j].y = data[i].y - temp_i;
				data[i].x += temp_r;
				data[i].y += temp_i;
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i].x /= (float)n;
			data[i].y /= (float)n;
		}
}