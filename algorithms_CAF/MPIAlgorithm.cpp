#include "stdafx.h"
#include "MPIAlgorithm.h"
#include <mpi.h>
#include <iostream>

/** ����������� �� ���������.*/
MPIAlgorithm::MPIAlgorithm()
{
}

/** ����������.*/
MPIAlgorithm::~MPIAlgorithm()
{
}

/** ������ ��������� ��������.
* @param N1 - ���������� ��� � ������� �������.
* @param N2 - ���������� ��� � ����������� �������.
* @param time_delay - ��������� �������� �������� �������.
* @param fo - ������� ������� �������� �/��� ������������ �������.
* @param Br - �������� ��������.
* @param T - ������ ���������.
* @param Fs - ������� ������������� �������� �/��� ������������ �������.
* @param dopler_freq - ������� ������� ��� ������������ �������.
* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
* @param d - ��������� ������/���(��).
* @param K - ��� ���������.
*/
void MPIAlgorithm::SetParams(int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs,
	float dopler_freq, float dopler_freq_for_ref_sig, float d, int K)
{
	_N1 = N1;
	_N2 = N2;
	_time_delay = time_delay;
	_fo = fo;
	_Br = Br;
	_T = T;
	_Fs = Fs;
	_dopler_freq = dopler_freq;
	_dopler_freq_for_ref_sig = dopler_freq_for_ref_sig;
	_d = d;
	_K = K;
}

/** ��������� ���.
* @param delay - �������� ��������� �������� �������� �������.
* @param max - ������������ �������� ��.
* @param QA - �������� ��������.
* @param argc - ����� ���������� ��������� ������.
* @param argv - ������ ���������� ��������� ������.
* @param K - ��� ���������.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
void MPIAlgorithm::CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	int ProcNum, ProcRank;
	int RecvDelay;
	float max_value;
	float Dispersion;
	MPI_Status Status;
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);

	/** ������ ������� ������������ �������.*/
	size_t size_vec_explr_sig = vec_explr.size();

	/** ������ ������� �������� �������.*/
	size_t size_vec_ref_sig = vec_ref.size();

	/** ������ ������� �������� ���.*/
	size_t size_CAF = size_vec_explr_sig - size_vec_ref_sig;

	/** �������� ������ ��� ���.*/
	std::vector<cufftComplex> vec_buf;
	vec_buf.resize(size_vec_ref_sig);

	std::vector<cufftComplex> vec_decim;
	vec_decim.resize(size_vec_ref_sig / K);

	/** ��������� ������ ��� �������.*/
	vec_amb_func.resize(size_CAF);
	for (size_t i = 0; i < size_CAF; i++)
	{
		vec_amb_func[i].resize(size_vec_ref_sig / K);
	}

	/** ���������� ��������� ����� �� ������ �� ���������
	* �� ������ �������� ����������� �������� ������� x �� i1 �� i2.
	*/
	int k = (int)size_CAF / ProcNum;
	int i1 = k * ProcRank;
	int i2 = k * (ProcRank + 1);
	if (ProcRank == ProcNum - 1)
		i2 = (int)size_CAF;

	float val_re, val_im;
	float maximum;
	std::vector<float> Vec_CAF_Buf;
	Vec_CAF_Buf.resize(size_CAF);
	/** ���������� ��.*/
	for (int i = i1; i < i2; i++)
	{
		val_re = 0;
		val_im = 0;
		std::fill(vec_buf.begin(), vec_buf.end(), cufftComplex{ 0, 0 });
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			vec_buf[j].x = (vec_ref[j].x * vec_explr[j + i].x +
				vec_ref[j].y * vec_explr[j + i].y);
			val_re += vec_buf[j].x;
			vec_buf[j].y = (vec_ref[j].x * (-vec_explr[j + i].y) +
				vec_ref[j].y * vec_explr[j + i].x);
			val_im += vec_buf[j].y;
		}

		val_re /= size_vec_ref_sig;
		val_im /= size_vec_ref_sig;

		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			vec_buf[j].x -= val_re;
			vec_buf[j].y -= val_im;
		}

		int k;
		int it = 0;
		size_t j = 0;
		std::fill(vec_decim.begin(), vec_decim.end(), cufftComplex{ 0, 0 });
		while (j < size_vec_ref_sig)
		{
			k = 0;
			while (k < K)
			{
				vec_decim[it].x += vec_buf[j].x;
				vec_decim[it].y += vec_buf[j].y;
				k++; j++;
			}
			it++;
		}

		maximum = 0;
		fourier(vec_decim.data(), (int)size_vec_ref_sig / K, -1);
		for (size_t k = 0; k < size_vec_ref_sig / K; k++)
		{
			vec_amb_func[i][k] = sqrt(vec_decim[k].x * vec_decim[k].x + vec_decim[k].y * vec_decim[k].y);
			if (maximum < vec_amb_func[i][k])
				maximum = vec_amb_func[i][k];
		}
		Vec_CAF_Buf[i] = maximum;
	}

	int max_idx;
	float Max = -0.3f;
	for (int it = i1; it < i2; it++)
	{
		if (Max < Vec_CAF_Buf[it])
		{
			Max = Vec_CAF_Buf[it];
			max_idx = it;
		}
	}
	/** ���������� �������� �� � ������������� ��������� [0;1].*/
	std::for_each(Vec_CAF_Buf.begin(), Vec_CAF_Buf.end(), [Max](float & x) { x /= Max; });

	/** �������������� ��������.*/
	float sum_avrg = 0;
	for (int i = i1; i < i2; ++i)
	{
		sum_avrg += Vec_CAF_Buf[i];
	}
	sum_avrg /= (i2 - i1);
	/** ���������.*/
	float D = 0;
	for (int i = i1; i < i2; ++i)
	{
		D += (Vec_CAF_Buf[i] - sum_avrg) * (Vec_CAF_Buf[i] - sum_avrg);
	}

	struct point
	{
		int n;
		float value;
		float disp;
	};

	if (ProcRank == 0)
	{
		point arr_delay[4];
		arr_delay[0].n = max_idx;
		arr_delay[0].value = Max;
		arr_delay[0].disp = D;
		for (int i = 1; i < ProcNum; i++)
		{
			MPI_Recv(&RecvDelay, 1, MPI_INT, i, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
			arr_delay[i].n = RecvDelay;
			MPI_Recv(&max_value, 1, MPI_FLOAT, i, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
			arr_delay[i].value = max_value;
			MPI_Recv(&Dispersion, 1, MPI_FLOAT, i, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
			arr_delay[i].disp = Dispersion;
		}

		max = -0.3f;
		for (int i = 0; i < ProcNum; ++i)
		{
			if (max < arr_delay[i].value)
			{
				max = arr_delay[i].value;
				delay = arr_delay[i].n;
			}
		}
		float tmp = 0;
		float sum_disp = 0;
		for (int i = 0; i < ProcNum; ++i)
		{
			arr_delay[i].value /= max;
			if (tmp < arr_delay[i].value)
				tmp = arr_delay[i].value;
			sum_disp += arr_delay[i].disp;
		}
		max = tmp;
		QA = max / sqrt(sum_disp);
	}
	else
	{
		MPI_Send(&max_idx, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&Max, 1, MPI_FLOAT, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&D, 1, MPI_FLOAT, 0, 0, MPI_COMM_WORLD);
	}

	MPI_Finalize();
}