#pragma once

#include <lib/ExportMacro.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <vector>

#define BATCH 1

/** ����, ����������� ������������ ������� ������� �������� � ������������ ��������.*/
__global__ void kernel_signal_mult(cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes);

/** ����, ����������� ������ ��� ����� ��������� �������� ��������.*/
__global__ void kernel_abs_fft(cufftComplex * pDevSigDec, float * pDevAbsFFT);

/** ����, ����������� ������������ ������.*/
__global__ void kernel_mul_matrix(cufftComplex *A, cufftComplex *B,
	int width_A, int width_B, cufftComplex *C);

/** ���� �������������� �������������� �������.*/
__global__ void kernel_matrix_transform(cufftComplex *till_M, cufftComplex *after_N, int width_M, int width_N);

/** ����, ����������� ������������ ������.*/
__global__ void kernel_mul_matrixEx(cufftComplex *A, cufftComplex *B,
	int width_A, int height_A, int width_B, cufftComplex *C);
