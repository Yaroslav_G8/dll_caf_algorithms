#include "stdafx.h"
#include "AlgUser.h"

/** ����������� �� ���������.*/
AlgUser::AlgUser()
{
}

/** ������������� ������� � ����������� �������.
* @param alg - ������ �� ������� �����.
* @param N1 - ���������� ��� � ������� �������.
* @param N2 - ���������� ��� � ����������� �������.
* @param time_delay - ��������� �������� �������� �������.
* @param fo - ������� ������� �������� �/��� ������������ �������.
* @param Br - �������� ��������.
* @param T - ������ ��������� (���������� Br).
* @param Fs - ������� ������������� �������� �/��� ������������ �������.
* @param dopler_freq - ������� ������� ��� ������������ �������.
* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
* @param d - ��������� ������/���(��).
* @param K - ��� ���������.
* @param BPSK_or_MSK - ���� BPSK_or_MSK = true, �� BPSK - ���������, ����� MSK - ���������.
* @param vec_ref - ������� ������ (�������� ��������).
* @param vec_explr - ����������� ������ (�������� ��������).
*/
void AlgUser::gen_sig(Algorithm * alg, int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs, float dopler_freq, float dopler_freq_for_ref_sig, float d, int K, bool BPSK_or_MSK,
	std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr)
{
	alg->SetParams(N1, N2, time_delay,
		fo, Br, T, Fs,
		dopler_freq, dopler_freq_for_ref_sig, d, K);
	/** ���.*/
	int delay = alg->Get_Delay();
	alg->CreateBitSignals(delay);
	alg->SelectModulation(BPSK_or_MSK, vec_ref, vec_explr);
}

/** ������������ ���������� �������� ���������� ���.*/
void AlgUser::calc(Algorithm * alg, int & delay, float & max, float & QA, int argc, char* argv[], int K,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	alg->CAF_Calculation(delay, max, QA, argc, argv, K, vec_ref, vec_explr);
}

/** ����������.*/
AlgUser::~AlgUser()
{
}
