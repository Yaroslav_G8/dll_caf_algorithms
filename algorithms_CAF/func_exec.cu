#include "cu_func.h"
#include "CUDAAlgorithm.h"
#include "CUDAMatrix.h"
#include "CudaMatrixEx.h"
#include <vector>
#include <iostream>

#define BLOCK_SIZE 16

/** ����, ����������� ������������ ������� ������� �������� � ������������ ��������.*/
__global__ void kernel_signal_mult(cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	pDevRes[index].x = pDevRefSig[index].x * pDevExplrSig[index].x + pDevRefSig[index].y * pDevExplrSig[index].y;
	pDevRes[index].y = pDevRefSig[index].x * (-pDevExplrSig[index].y) + pDevRefSig[index].y * pDevExplrSig[index].x;
}

/** ����, ����������� ������ ��� ����� ��������� �������� ��������.*/
__global__ void kernel_abs_fft(cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	pDevAbsFFT[index] = sqrt(pDevSigDec[index].x * pDevSigDec[index].x + pDevSigDec[index].y * pDevSigDec[index].y);
}

/** ����, ����������� ������������ ������.*/
__global__ void kernel_mul_matrix(cufftComplex *A, cufftComplex *B,
	int width_A, int width_B, cufftComplex *C)
{
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	/** ������ ������ ���������� �������� ������� �, �������������� ������.*/
	int aBegin = width_A * BLOCK_SIZE * by;
	/** ������ ��������� ���������� �������� ������� �, �������������� ������.*/
	int aEnd = aBegin + width_A - 1;
	/** ���, ������������ ��� ������������ �� ����������� �������� ������� �.*/
	int aStep = BLOCK_SIZE;
	/** ������ ������ ���������� �������� ������� �, �������������� ������.*/
	int bBegin = BLOCK_SIZE * bx;
	/** ���, ������������ ��� ������������ �� ����������� �������� ������� �.*/
	int bStep = BLOCK_SIZE * width_B;
	/** The element of the block sub-matrix that is computed by the thread.*/
	cufftComplex Csub;
	Csub.x = 0; Csub.y = 0;
	for (int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep)
	{
		// ����������� ������ ��� ���������� �
		__shared__ cufftComplex As[BLOCK_SIZE][BLOCK_SIZE];
		// ����������� ������ ��� ���������� B
		__shared__ cufftComplex Bs[BLOCK_SIZE][BLOCK_SIZE];
		As[ty][tx] = A[a + width_A * ty + tx];   // Load the matrices from global memory to shared memory;
		Bs[ty][tx] = B[b + width_B * ty + tx];   // each thread loads one element of each matrix
		__syncthreads(); // Synchronize to make sure the matrices are loaded
						 // Multiply the two matrices together;
						 // each thread computes one element
						 // of the block sub-matrix
		for (int k = 0; k < BLOCK_SIZE; ++k)
		{
			Csub.x += (As[ty][k].x * Bs[k][tx].x + As[ty][k].y * Bs[k][tx].y);
			Csub.y += (As[ty][k].x * Bs[k][tx].y - As[ty][k].y * Bs[k][tx].x);
		}
		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}
	// Write the block sub-matrix to global memory;
	// each thread writes one element
	int c = width_B * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + width_B * ty + tx] = Csub;
}

/** ���� �������������� �������������� �������.*/
__global__ void kernel_matrix_transform(cufftComplex *M, cufftComplex *N, int width_M, int width_N)
{
	int j_N = blockIdx.x * blockDim.x + threadIdx.x;
	int i_N = blockIdx.y * blockDim.y + threadIdx.y;

	N[i_N * width_N + j_N] = M[(i_N + j_N) * width_N + j_N];
}

/** ����, ����������� ������������ ������.*/
__global__ void kernel_mul_matrixEx(cufftComplex *A, cufftComplex *B,
	int width_A, int height_A, int width_B, cufftComplex *C)
{
	/** Index of current block in X.*/
	int bx = blockIdx.x;
	/** Index of current block in Y.*/
	int by = blockIdx.y;
	/** Index of current thread in X.*/
	int tx = threadIdx.x;
	/** Index of current thread in Y.*/
	int ty = threadIdx.y;
	/** ������ ������ ���������� �������� ������� �, �������������� ������.*/
	int aBegin = by * width_A * BLOCK_SIZE + bx * width_A;
	/** ������ ��������� ���������� �������� ������� �, �������������� ������.*/
	int aEnd = aBegin + width_A;
	/** ������ ������ ���������� �������� ������� �, �������������� ������.*/
	int bBegin = bx * width_A;
	/** The element of the block sub-matrix that is computed by the thread.*/
	cufftComplex Csub;
	Csub.x = 0.0f; Csub.y = 0.0f;
	__shared__ cufftComplex As[BLOCK_SIZE][BLOCK_SIZE + 1];
	__shared__ cufftComplex Bs[BLOCK_SIZE];

#pragma unroll
	for (int a = aBegin, b = bBegin; a < aEnd; a += BLOCK_SIZE, b += BLOCK_SIZE)
	{
		As[tx][ty] = A[a + width_A * ty + tx];   // Load the matrices from global memory to shared memory;
		Bs[tx] = B[b + tx];   // each thread loads one element of each matrix
		__syncthreads(); // Synchronize to make sure the matrices are loaded
						 // Multiply the two matrices together;
						 // each thread computes one element
						 // of the block sub-matrix
		for (int k = 0; k < BLOCK_SIZE; ++k)
		{
			Csub.x += (As[k][tx].x * Bs[k].x + As[k][tx].y * Bs[k].y);
			Csub.y += (As[k][tx].x * Bs[k].y - As[k][tx].y * Bs[k].x);
		}
		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}
	// Write the block sub-matrix to global memory;
	// each thread writes one element

	int ic = bx * (height_A - width_B) + by * BLOCK_SIZE + tx;
	// Store the element in global memory
	if (ty == 0) // this condition leads to branching, 
		// code without this condition works, but 
		// it is not quite correct to not use this condition due to the "Race condition"
	{
		C[ic] = Csub;
	}
}

/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
void CUDAAlgorithm::signal_mult(int numBlocks, int blockSize,
	cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes)
{
	kernel_signal_mult << <numBlocks, blockSize >> > (pDevRefSig, pDevExplrSig, pDevRes);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
void CUDAMatrix::signal_mult(int numBlocks, int blockSize,
	cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes)
{
	kernel_signal_mult << <numBlocks, blockSize >> > (pDevRefSig, pDevExplrSig, pDevRes);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
void CudaMatrixEx::signal_mult(int numBlocks, int blockSize,
	cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes)
{
	kernel_signal_mult << <numBlocks, blockSize >> > (pDevRefSig, pDevExplrSig, pDevRes);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
void CUDAAlgorithm::abs_fft(int numBlocks, int blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	kernel_abs_fft << <numBlocks, blockSize >> > (pDevSigDec, pDevAbsFFT);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
void CUDAMatrix::abs_fft(int numBlocks, int blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	kernel_abs_fft << <numBlocks, blockSize >> > (pDevSigDec, pDevAbsFFT);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
void CUDAMatrix::abs_fft_matrix(int numBlocks, int blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	kernel_abs_fft << <numBlocks, blockSize >> > (pDevSigDec, pDevAbsFFT);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
void CudaMatrixEx::abs_fft(int numBlocks, int blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	kernel_abs_fft << <numBlocks, blockSize >> > (pDevSigDec, pDevAbsFFT);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
void CudaMatrixEx::abs_fft_matrix(int numBlocks, int blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	kernel_abs_fft << <numBlocks, blockSize >> > (pDevSigDec, pDevAbsFFT);
}

void CUDAMatrix::mul_matrix(cufftComplex *A, cufftComplex *B,
	int width_A, int height_A, int width_B, cufftComplex *C)
{
	// Compute the execution configuration assuming the matrix dimensions are multiples of BLOCK_SIZE
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(width_B / dimBlock.x, height_A / dimBlock.y);
	kernel_mul_matrix << <dimGrid, dimBlock >> > (A, B, width_A, width_B, C);
}

void CudaMatrixEx::mul_matrix(cufftComplex *A, cufftComplex *B,
	int width_A, int height_A, int width_B, cufftComplex *C)
{
	// Compute the execution configuration assuming the matrix dimensions are multiples of BLOCK_SIZE
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(width_B, (height_A - width_B) / dimBlock.y);
	kernel_mul_matrixEx << <dimGrid, dimBlock >> > (A, B, width_A, height_A, width_B, C);
}

/** �������, � ������� ����������� �� ���������� ���� kernel_matrix_transform.*/
void CUDAMatrix::matrix_transform(cufftComplex *M, cufftComplex *N, 
	int width_M, int height_M,
	int width_N, int height_N)
{
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(width_N / dimBlock.x, height_N / dimBlock.y);
	kernel_matrix_transform << <dimGrid, dimBlock >> > (M, N, width_M, width_N);
}
