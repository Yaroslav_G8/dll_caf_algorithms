#include "stdafx.h"
#include "SeqAlgorithm.h"
#include <algorithm>
#include <iostream>

/** ����������� �� ���������.*/
SeqAlgorithm::SeqAlgorithm()
{
}

/** ����������.*/
SeqAlgorithm::~SeqAlgorithm()
{
}

/** ������ ��������� ��������.
* @param N1 - ���������� ��� � ������� �������.
* @param N2 - ���������� ��� � ����������� �������.
* @param time_delay - ��������� �������� �������� �������.
* @param fo - ������� ������� �������� �/��� ������������ �������.
* @param Br - �������� ��������.
* @param T - ������ ���������.
* @param Fs - ������� ������������� �������� �/��� ������������ �������.
* @param dopler_freq - ������� ������� ��� ������������ �������.
* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
* @param d - ��������� ������/���(��).
* @param K - ��� ���������.
*/
void SeqAlgorithm::SetParams(int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs,
	float dopler_freq, float dopler_freq_for_ref_sig, float d, int K)
{
	_N1 = N1;
	_N2 = N2;
	_time_delay = time_delay;
	_fo = fo;
	_Br = Br;
	_T = T;
	_Fs = Fs;
	_dopler_freq = dopler_freq;
	_dopler_freq_for_ref_sig = dopler_freq_for_ref_sig;
	_d = d;
	_K = K;
}

/** ��������� ���.
* @param delay - �������� ��������� �������� �������� �������.
* @param max - ������������ �������� ��.
* @param QA - �������� ��������.
* @param argc - ����� ���������� ��������� ������.
* @param argv - ������ ���������� ��������� ������.
* @param K - ��� ���������.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
void SeqAlgorithm::CAF_Calculation(int & delay, float & max, float & QA, int argc, char* argv[], int K,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr)
{
	/** ������ ������� ������������ �������.*/
	size_t size_vec_explr_sig = vec_explr.size();

	/** ������ ������� �������� �������.*/
	size_t size_vec_ref_sig = vec_ref.size();

	/** ������ ������� ���.*/
	size_t size_CAF = size_vec_explr_sig - size_vec_ref_sig;

	/** �������� ������ ��� ���.*/
	std::vector<cufftComplex> vec_buf;
	vec_buf.resize(size_vec_ref_sig);

	std::vector<cufftComplex> vec_decim;
	vec_decim.resize(size_vec_ref_sig / K);

	/** ��������� ������ ��� ������� ���.*/
	vec_amb_func.resize(size_CAF);
	for (size_t i = 0; i < size_CAF; i++)
	{
		vec_amb_func[i].resize(size_vec_ref_sig / K);
	}

	float val_re, val_im;
	float max1 = 0;
	Vec_CAF.clear();
	/** ���������� ��.*/
	for (size_t i = 0; i < size_CAF; i++)
	{
		val_re = 0;
		val_im = 0;
		std::fill(vec_buf.begin(), vec_buf.end(), cufftComplex{ 0, 0 });
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			vec_buf[j].x = (vec_ref[j].x * vec_explr[j + i].x +
				vec_ref[j].y * vec_explr[j + i].y);
			val_re += vec_buf[j].x;
			vec_buf[j].y = (vec_ref[j].x * (-vec_explr[j + i].y) +
				vec_ref[j].y * vec_explr[j + i].x);
			val_im += vec_buf[j].y;
		}

		val_re /= size_vec_ref_sig;
		val_im /= size_vec_ref_sig;

		// ����������
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			vec_buf[j].x -= val_re;
			vec_buf[j].y -= val_im;
		}

		int k;
		int it = 0;
		size_t j = 0;
		std::fill(vec_decim.begin(), vec_decim.end(), cufftComplex{ 0, 0 });
		while (j < size_vec_ref_sig)
		{
			k = 0;
			while (k < K)
			{
				vec_decim[it].x += vec_buf[j].x;
				vec_decim[it].y += vec_buf[j].y;
				k++; j++;
			}
			it++;
		}

		max1 = 0;
		fourier(vec_decim.data(), int(size_vec_ref_sig) / K, -1);
		for (size_t k = 0; k < size_vec_ref_sig / K; k++)
		{
			vec_amb_func[i][k] = sqrt(vec_decim[k].x * vec_decim[k].x + vec_decim[k].y * vec_decim[k].y);
			if (max1 < vec_amb_func[i][k])
				max1 = vec_amb_func[i][k];
		}
		Vec_CAF.push_back(max1);
	}

	max = 0;
	for (size_t it = 0; it < Vec_CAF.size(); it++)
	{
		if (max < Vec_CAF[it])
		{
			max = Vec_CAF[it];
			delay = (int)it;
		}
	}

	/** ���������� �������� �� � ������������� ��������� [0;1].*/
	std::for_each(Vec_CAF.begin(), Vec_CAF.end(), [max](float & x) { x /= max; });

	/** �������������� ��������.*/
	float sum_avrg = 0;
	for (size_t i = 0; i < size_CAF; ++i)
	{
		sum_avrg += Vec_CAF[i];
	}
	sum_avrg /= size_CAF;
	/** ���������.*/
	float D = 0;
	for (size_t i = 0; i < size_CAF; ++i)
	{
		D += (Vec_CAF[i] - sum_avrg) * (Vec_CAF[i] - sum_avrg);
	}
	max = 0;
	for (size_t j = 0; j < size_CAF; ++j)
	{
		if (max < Vec_CAF[j])
		{
			max = Vec_CAF[j];
		}
	}
	QA = max / sqrt(D);
}
