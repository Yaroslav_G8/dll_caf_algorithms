#pragma once
#include<vector>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>

#ifdef ALGORITHMS_DLL_EXPORTS
	#define ALGORITHMS_DLL_API __declspec(dllexport) 
#else
	#define ALGORITHMS_DLL_API __declspec(dllimport) 
#endif

/** ������������� ������� � ����������� �������.
* @param AlgNum - ��������, ���������� �� �������� ���������� ���:
* AlgNum = 1 - ���������������� �������� ���������� ���.
* AlgNum = 2 - ������������ �������� ���������� ��� � ������� MPI.
* AlgNum = 3 - ������������ �������� ���������� ��� � ������� CUDA.
* AlgNum = 4 - ������������ �������� ���������� ��� � ������� CUDAMatrix.
* @param N1 - ���������� ��� � ������� �������.
* @param N2 - ���������� ��� � ����������� �������.
* @param time_delay - ��������� �������� �������� �������.
* @param fo - ������� ������� �������� �/��� ������������ �������.
* @param Br - �������� ��������.
* @param T - ������ ��������� (���������� 1.0f / Br).
* @param Fs - ������� ������������� �������� �/��� ������������ ������� (���).
* @param dopler_freq - ������� ������� ��� ������������ �������.
* @param dopler_freq_for_ref_sig - ������� ������� ��� �������� �������.
* @param d - ��������� ������/���(��).
* @param K - ��� ���������.
* @param BPSK_or_MSK - ���� BPSK_or_MSK = true, �� BPSK - ���������, ����� MSK - ���������.
* @param vec_ref - ������� ������ (�������� ��������).
* @param vec_explr - ����������� ������ (�������� ��������).
*/
ALGORITHMS_DLL_API void generate_signals(int AlgNum, int N1, int N2, int time_delay,
	float fo, int Br, float T, float Fs, float dopler_freq, float dopler_freq_for_ref_sig, float d, int K, bool BPSK_or_MSK,
	std::vector<cufftComplex> & vec_ref, std::vector<cufftComplex> & vec_explr);

/** ��������� ���.
* @param AlgNum - ��������, ���������� �� �������� ���������� ���:
* AlgNum = 1 - ���������������� �������� ���������� ���.
* AlgNum = 2 - ������������ �������� ���������� ��� � ������� MPI.
* AlgNum = 3 - ������������ �������� ���������� ��� � ������� CUDA.
* AlgNum = 4 - ������������ �������� ���������� ��� � ������� CUDAMatrix.
* @param delay - ���, ��������������� ��������� ��� (�������� ��������).
* @param max - �������� ��� (�������� ��������).
* @param argc - ����� ���������� ��������� ������.
* @param argv - ��������� �� ������ ���������� ��������� ������.
* @param K - ��� ���������.
* @param QA - �������� �������� ���.
* @param vec_ref - ������� ������.
* @param vec_explr - ����������� ������.
*/
ALGORITHMS_DLL_API void calculation(int AlgNum, int & delay, float & max, int argc, char* argv[], int K, float & QA,
	const std::vector<cufftComplex> & vec_ref, const std::vector<cufftComplex> & vec_explr);