// impfunc.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <lib\ExportMacro.h>
#include <iostream>
#include <vector>
#include <chrono>

int main(int argc, char* argv[])
{
	enum class Algorithm
	{
		Serial = 1,
		Mpi = 2,
		CudaSerial = 3,
		CudaMatrix = 4,
		CudaMatrixEx = 5
	};

	std::vector<cufftComplex> vec_ref, vec_explr;
	generate_signals(
		static_cast<int>(Algorithm::CudaMatrix),	// алгоритм 
		320,										// длина опорного сигнала
		640,										// длина исследуемого сигнала
		10,											// ВВЗ
		25,											// несущая частота
		5000000,									// битовая скорость
		1.0f / 5000000,								// период модуляции
		500,											// частота дискретизации 
		10,											// частота Доплера для опорного
		0,											// частота Доплера для исследуемого сигнала
		10,											// процент шума в сигнале
		128,											// шаг децимации
		true,										// тип модуляции
		vec_ref,									// опорный сигнал
		vec_explr									// исследуемый сигнал
	);
	
	int delay;
	float max_CAF;
	float QA;

	auto start = std::chrono::high_resolution_clock::now();
	calculation(static_cast<int>(Algorithm::CudaMatrix), delay, max_CAF, argc, argv, 128, QA, vec_ref, vec_explr);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << "Max: " << max_CAF << std::endl;
	std::cout << "Time delay: " << delay << std::endl;
	std::cout << "Quality criterion: " << QA << std::endl;

	std::cout << "CAF execution time: ";
	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;

	vec_explr.clear();
	vec_ref.clear();

    return 0;
}